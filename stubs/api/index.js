const router = require('express').Router();

router.get('/person/:id', (req, res) => {
  let response = require('./person.json');
  res.send(response);
});

router.get('/culture', (req, res) => {
  let response = require('./cultures.json');
  res.send(response);
});

router.get('/culture/:id', (req, res) => {
  let response = require('./culture.json');
  res.send(response);
});

router.get('/object', (req, res) => {
  let response = require('./object.json');
  res.send(response);
});

router.get('/object/:id', (req, res) => {
  let response = require('./object.json');
  res.send(response.records[0]);
});

router.get('/century', (req, res) => {
  let response = require('./centuries.json');
  res.send(response);
});

router.get('/century/:id', (req, res) => {
  let response = require('./centuries.json');
  res.send(response.records[0]);
});

router.get('/classification', (req, res) => {
  let response = require('./classifications.json');
  res.send(response);
});

router.get('/classification/:id', (req, res) => {
  let response = require('./classifications.json');
  res.send(response.records[0]);
});

module.exports = router;

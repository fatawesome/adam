const pkg = require('./package.json');

module.exports = {
  apiPath: 'stubs/api',
  webpackConfig: {
    output: {
      publicPath: `/static/adam/${pkg.version}/`,
    },
    resolve: {
      extensions: ['.jsx'],
    },
    module: {
      rules: [
        { parser: { system: false } },
        {
          test: /\.tsx?$/,
          loader: 'awesome-typescript-loader',
        },
        {
          test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/,
          loader: 'file-loader',
        },
        {
          test: /\.css$/i,
          exclude: /node_modules/,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                modules: true,
              },
            },
          ],
        },
      ],
    },
  },
  navigations: {
    repos: '/adam',
  },
  config: {
    'adam.api.base': '/api',
  },
};

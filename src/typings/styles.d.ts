declare module '*.module.css' {
  const classes: { [key: string]: string };
  // noinspection JSDuplicatedDeclaration
  export default classes;
}

import { Item } from '@main/__data__/model/item';

export const DEFAULT_IMAGE_URL = 'https://upload.wikimedia.org/wikipedia/en/2/29/Harvard_shield_wreath.svg';

export function toImageURL(url: string, width?: number, height?: number): string {
  return `${url}?height=${height || 640}&width=${width || 640}`;
}

export function getImage(item: Partial<Item>): string {
  return item.primaryimageurl || item.baseimageurl || DEFAULT_IMAGE_URL;
}

import { toImageURL, getImage, DEFAULT_IMAGE_URL } from '../index';

describe('utils', () => {
  describe('toImageURL', () => {
    it('sets width and height', () => {
      const url = 'protocol://domain/path';
      expect(toImageURL(url, 100, 100)).toBe(`${url}?height=100&width=100`);
    });
  });

  describe('getImage', () => {
    describe('if there are no images', () => {
      it('returns default image url', () => {
        expect(getImage({})).toBe(DEFAULT_IMAGE_URL);
      });
    });
  });
});

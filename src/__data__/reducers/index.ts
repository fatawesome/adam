import { combineReducers } from 'redux';
import { culture, CultureState, cultures, CulturesListState } from './culture';

export type StateType = {
  culture: CultureState;
  cultures: CulturesListState;
};

export default combineReducers({
  culture,
  cultures,
});

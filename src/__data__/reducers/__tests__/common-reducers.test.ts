import { fetchHandler, fetchSuccessHandler, fetchErrorHandler, DEFAULT_ERROR } from '../common';
import { DataState } from '../../model/common';

interface Entity {
  field: string;
}

const state = {
  loading: false,
  data: null,
  error: null,
};

const action = {
  data: 'some data',
  type: 'some type',
};

describe('src/__data__/reducers/common', () => {
  describe('fetchHandler', () => {
    it('should return loading state', () => {
      expect(fetchHandler<DataState<Entity>>(state)).toStrictEqual({
        ...state,
        loading: true,
      });
    });
  });

  describe('fetchSuccessHandler', () => {
    it('should return loading state', () => {
      expect(fetchSuccessHandler<Entity>(state, action)).toStrictEqual({
        ...state,
        data: action.data,
        loading: false,
      });
    });
  });

  describe('fetchErrorHandler', () => {
    it('should return default error state', () => {
      expect(fetchErrorHandler<Entity>(state, action)).toStrictEqual({
        ...state,
        data: null,
        loading: false,
        error: DEFAULT_ERROR,
      });
    });

    it('should return custom error state', () => {
      const errorAction = {
        type: 'some type',
        error: 'custom error',
      };
      expect(fetchErrorHandler<Entity>(state, errorAction)).toStrictEqual({
        ...state,
        data: null,
        loading: false,
        error: errorAction.error,
      });
    });
  });
});

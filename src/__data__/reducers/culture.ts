import { AnyAction } from 'redux';
import {
  CULTURE_FETCH,
  CULTURE_FETCH_SUCCESS,
  CULTURE_FETCH_FAIL,
  CULTURES_LIST_FETCH,
  CULTURE_LIST_FETCH_SUCCESS,
  CULTURE_LIST_FETCH_FAIL,
} from '@main/__data__/constants/action-types';
import { DataState } from '@main/__data__/model/common';
import { Culture, CulturesListWithPagination } from '@main/__data__/model/culture';
import { fetchErrorHandler, fetchHandler, fetchSuccessHandler } from '@main/__data__/reducers/common';

export type CultureState = DataState<Culture>;
export type CulturesListState = DataState<CulturesListWithPagination>;

const initialCultureState: CultureState = {
  data: null,
  loading: false,
  error: null,
};

const initialCulturesListState: CulturesListState = {
  data: null,
  loading: false,
  error: null,
};

const cultureHandlers = {
  [CULTURE_FETCH]: fetchHandler,
  [CULTURE_FETCH_SUCCESS]: fetchSuccessHandler,
  [CULTURE_FETCH_FAIL]: fetchErrorHandler,
};

const culturesListHandlers = {
  [CULTURES_LIST_FETCH]: fetchHandler,
  [CULTURE_LIST_FETCH_SUCCESS]: fetchSuccessHandler,
  [CULTURE_LIST_FETCH_FAIL]: fetchErrorHandler,
};

export const culture = (state: CultureState = initialCultureState, action: AnyAction) => {
  return cultureHandlers[action.type] ? cultureHandlers[action.type]<Culture>(state, action) : state;
};

export const cultures = (state: CulturesListState = initialCulturesListState, action: AnyAction) => {
  return culturesListHandlers[action.type]
    ? culturesListHandlers[action.type]<CulturesListWithPagination>(state, action)
    : state;
};

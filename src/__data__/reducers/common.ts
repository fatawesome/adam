import { AnyAction } from 'redux';
import { DataState } from '@main/__data__/model/common';

export const DEFAULT_ERROR = 'Something went wrong';

export const fetchHandler = <T>(state: T): DataState<T> => ({
  ...state,
  loading: true,
});

export const fetchSuccessHandler = <T>(state: DataState<T>, action: AnyAction): DataState<T> => ({
  ...state,
  data: action?.data,
  loading: false,
});

export const fetchErrorHandler = <T>(state: DataState<T>, action: AnyAction): DataState<T> => ({
  ...state,
  data: null,
  loading: false,
  error: action.error || DEFAULT_ERROR,
});

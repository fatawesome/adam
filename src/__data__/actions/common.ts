import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { getConfig } from '@ijl/cli';

export const fetchAction = (type) => ({ type });
export const successAction = (type, data) => ({ type, data });
export const errorAction = (type, error) => ({ type, error });

const axiosConfig: AxiosRequestConfig = {
  baseURL: getConfig()['adam.api.base'],
  headers: { 'Content-Type': 'application/json' },
  params: {
    apikey: '9f8c605c-9348-4a13-b0d8-ce8f7eb6fea5',
  },
};

export const api: AxiosInstance = axios.create(axiosConfig);

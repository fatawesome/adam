import { Dispatch } from 'redux';
import { AxiosResponse } from 'axios';

import {
  CULTURE_FETCH,
  CULTURE_FETCH_SUCCESS,
  CULTURE_FETCH_FAIL,
  CULTURES_LIST_FETCH,
  CULTURE_LIST_FETCH_SUCCESS,
  CULTURE_LIST_FETCH_FAIL,
} from '@main/__data__/constants/action-types';
import { api, errorAction, fetchAction, successAction } from '@main/__data__/actions/common';
import { Culture, CulturesListWithPagination } from '@main/__data__/model/culture';
import { toImageURL } from '@main/utils';

async function getCultureImageURL(id) {
  try {
    const res = await api.get('/object', {
      params: { culture: id, hasimage: 1, sort: 'random', size: 1 },
    });
    return res.data.records[0].images[0].baseimageurl;
  } catch (e) {
    return 'https://upload.wikimedia.org/wikipedia/en/2/29/Harvard_shield_wreath.svg';
  }
}

async function getCultureObjects(id) {
  const res = await api.get('/object', {
    params: { culture: id, hasimage: 1, sort: 'random', size: 8 },
  });
  return res.data.records.map((el) => {
    return {
      ...el,
      name: el.title,
      imageURL: toImageURL(
        el.baseimageurl ||
          el.primaryimageurl ||
          'https://upload.wikimedia.org/wikipedia/en/2/29/Harvard_shield_wreath.svg',
        300,
        300,
      ),
    };
  });
}

export const getCulture = (id: number) => async (dispatch: Dispatch) => {
  dispatch(fetchAction(CULTURE_FETCH));
  try {
    const res: AxiosResponse<Culture> = await api.get(`/culture/${id}`);
    if (res.status === 200) {
      res.data.imageURL = await getCultureImageURL(res.data.id);
      res.data.objects = await getCultureObjects(res.data.id);
      dispatch(successAction(CULTURE_FETCH_SUCCESS, res.data));
    } else {
      dispatch(errorAction(CULTURE_FETCH_FAIL, 'Cannot load culture'));
    }
  } catch (e) {
    dispatch(errorAction(CULTURE_FETCH_FAIL, e.message || 'Cannot load culture'));
  }
};

export const getCulturesList = () => async (dispatch: Dispatch) => {
  dispatch(fetchAction(CULTURES_LIST_FETCH));
  try {
    const res: AxiosResponse<CulturesListWithPagination> = await api.get('/culture?size=8&sort=random');
    if (res.status === 200) {
      res.data.records = await Promise.all(
        res.data.records.map(async (el) => {
          return {
            ...el,
            imageURL: await getCultureImageURL(el.id),
          };
        }),
      );
      dispatch(successAction(CULTURE_LIST_FETCH_SUCCESS, res.data));
    } else {
      dispatch(errorAction(CULTURE_LIST_FETCH_FAIL, 'Cannot load cultures'));
    }
  } catch (e) {
    dispatch(errorAction(CULTURE_LIST_FETCH_FAIL, e.message || 'Cannot load cultures'));
  }
};

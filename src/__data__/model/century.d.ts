export interface Century {
  id: number;
  objectcount: number;
  lastupdate: string;
  temporalorder: number;
  name: string;
}

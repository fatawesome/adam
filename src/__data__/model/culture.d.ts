import { DataRequest } from '@main/__data__/model/common';

export interface Culture {
  objects?: Array<{
    id: number;
    name: string;
    imageURL?: string;
  }>;
  id: number;
  name: string;
  objectcount: number;
  lastupdate: string;
  imageURL?: string;
}
export type CultureDataRequest = DataRequest<Culture>;

export type CulturesList = Array<Culture>;
export type CulturesListWithPagination = {
  info: {
    totalrecordsperquery: number;
    totalrecords: number;
    pages: number;
    page: number;
    next: string;
  };
  records: CulturesList;
};
export type CulturesListDataRequest = DataRequest<CulturesListWithPagination>;

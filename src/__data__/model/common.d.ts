export interface AsyncDataState<T, E = string> {
  loading: boolean;
  data?: T;
  error?: E;
}

export interface DataRequest<T> {
  status: Status;
  data: T;
}

export type DataState<T> = Partial<DataRequest<T>> & AsyncDataState<T>;

interface Status {
  code: number;
}

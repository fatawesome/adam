export interface Item {
  copyright: boolean;
  otaluniquepageviews: number;
  contact: string;
  id: number;
  name?: string;
  verificationleveldescription: string;
  period: string;
  imagecount: number;
  classification: string;
  primaryimageurl: string;
  baseimageurl: string;
  style: string;
  lastupdate: string;
  commentary: string;
  technique?: string;
  description?: string;
  medium: string;
  title: string;
  dated: string;
  department: string;
  url: string;
  century: string;
  culture: string;
  loading?: boolean;
}

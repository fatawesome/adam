export interface Classification {
  objectcount: number;
  id: number;
  lastupdate: string;
  name: string;
  classificationid: number;
}

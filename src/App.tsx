import React, { Suspense } from 'react';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import * as Thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MetaTags from 'react-meta-tags';

import './styles.css';
import reducer from '@main/__data__/reducers';

import Content from '@main/components/containers/Content';
import Main from '@main/components/pages/Main';
import Culture from '@main/components/pages/Culture';
import Item from '@main/components/pages/Item';
import Search from '@main/components/pages/Search';

import Header from '@main/components/common/Header';
import { Loader } from '@main/components/common/Loader';

const App = () => (
  <Provider store={createStore(reducer, composeWithDevTools(applyMiddleware(Thunk.default)))}>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
    <Router basename="/adam">
      <MetaTags>
        <title>Harvard Art Museums</title>
        <meta name="description" content="Browse for tons of art content." />
        <meta property="og:title" content="Main" />
      </MetaTags>
      <Header />
      <Suspense fallback={Loader}>
        <Content>
          <Switch>
            <Route path="/search" component={Search} />
            <Route path="/item/:id/" component={Item} />
            <Route path="/culture/:id/" component={Culture} />
            <Route path="/" component={Main} />
          </Switch>
        </Content>
      </Suspense>
    </Router>
  </Provider>
);

export default App;

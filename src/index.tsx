import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

export const mount = (Component: React.ComponentType): void => {
  ReactDOM.render(<Component />, document.getElementById('app'));
};

export const unmount = (): void => {
  ReactDOM.unmountComponentAtNode(document.getElementById('app'));
};

export default App;

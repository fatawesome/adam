import React from 'react';
import { render, screen } from '@testing-library/react';
import MockAdapter from 'axios-mock-adapter';
import { api } from '../../../../__data__/actions/common';
import { BrowserRouter } from 'react-router-dom';
import CulturesList from '../index';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import reducer from '@main/__data__/reducers';
import * as Thunk from 'redux-thunk';

describe('src/components/features/CultureList', () => {
  let mock = null;
  beforeEach(() => {
    mock = new MockAdapter(api);
    mock.onGet('/culture?size=8&sort=random').reply(200, require('../../../../../stubs/api/cultures.json'));
  });

  describe('before getting data', () => {
    mock = new MockAdapter(api, { delayResponse: 500 });
    it('renders loader', () => {
      render(
        <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
          <BrowserRouter>
            <CulturesList />
          </BrowserRouter>
        </Provider>,
      );
      expect(screen.findByTestId('loader')).toBeDefined();
    });
  });

  describe('after getting data', () => {
    it('renders culture list', async () => {
      render(
        <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
          <BrowserRouter>
            <CulturesList />
          </BrowserRouter>
        </Provider>,
      );
      expect(await screen.findByTestId('object_list')).toBeDefined();
    });
  });
});

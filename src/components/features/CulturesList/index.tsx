import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { StateType } from '@main/__data__/reducers';
import { getCulturesList } from '@main/__data__/actions/culture';

import { Loader } from '@main/components/common/Loader';
import ObjectList from '@main/components/containers/ObjectList';

const CulturesList = () => {
  const dispatch = useDispatch();
  const culturesData = useSelector((state: StateType) => {
    return state.cultures;
  });

  useEffect(() => {
    dispatch(getCulturesList());
  }, []);

  if (culturesData.error) {
    return <h2>Error happened</h2>;
  }

  if (!culturesData.data || culturesData.loading) {
    return <Loader />;
  }

  return <ObjectList data={culturesData.data.records} linkBase={'culture'} />;
};

export default CulturesList;

import React from 'react';
import CulturesList from '@main/components/features/CulturesList';

const Main = () => {
  return <CulturesList />;
};

export default Main;

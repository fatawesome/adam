import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { render, screen, fireEvent } from '@testing-library/react';
import { api } from '../../../../__data__/actions/common';
import { BrowserRouter } from 'react-router-dom';

import Search from '../index';

describe('src/components/pages/Search', () => {
  let mock = null;
  beforeEach(() => {
    mock = new MockAdapter(api, { delayResponse: 500 });
    mock.onGet('/object').reply(200, require('../../../../../stubs/api/object.json'));
    mock.onGet(/\/culture\/*/).reply(200, require('../../../../../stubs/api/cultures.json'));
    mock.onGet(/\/classification\/*/).reply(200, require('../../../../../stubs/api/classifications.json'));
    mock.onGet(/\/century\/*/).reply(200, require('../../../../../stubs/api/centuries.json'));
  });

  describe('before loading search parameters', () => {
    it('renders loader', async () => {
      render(<Search />);
      expect(await screen.findByTestId('loader')).toBeDefined();
      expect(await screen.findByTestId('submit')).toBeDefined();
    });
  });

  describe('before making any search', () => {
    it('renders only form', async () => {
      render(<Search />);
      expect(await screen.findByTestId('submit')).toBeDefined();
      expect(screen.queryByTestId('search_results')).toBeNull();
    });
  });

  describe('after performing search', () => {
    it('renders results', async () => {
      render(
        <BrowserRouter>
          <Search />
        </BrowserRouter>,
      );
      const button = await screen.findByTestId('submit');
      fireEvent(
        button,
        new MouseEvent('click', {
          bubbles: true,
          cancelable: true,
        }),
      );
      expect(await screen.findByTestId('search_results')).toBeDefined();
    });
  });
});

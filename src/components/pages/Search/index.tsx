import React, { useEffect, useState } from 'react';

import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import styles from './styles.module.css';

import { api } from '@main/__data__/actions/common';
import { Culture } from '@main/__data__/model/culture';
import { Loader } from '@main/components/common/Loader';
import { Century } from '@main/__data__/model/century';
import { Classification } from '@main/__data__/model/classification';
import ObjectList from '@main/components/containers/ObjectList';
import { Item } from '@main/__data__/model/item';
import { toImageURL } from '@main/utils';

const Search = () => {
  const [error, setError] = useState<Error | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  const [centuries, setCenturies] = useState<Array<Century>>([]);
  const [centuryList, setCenturyList] = useState<Array<Century>>([]);
  const onCenturiesChange = (event, values) => {
    setCenturies(values);
  };

  const [classifications, setClassifications] = useState<Array<Classification>>([]);
  const [classificationList, setClassificationList] = useState<Array<Classification>>([]);
  const onClassificationsChange = (event, values) => {
    setClassifications(values);
  };

  const [cultures, setCultures] = useState<Array<Culture>>([]);
  const [cultureList, setCultureList] = useState<Array<Culture>>([]);
  const onCulturesChange = (event, values) => {
    setCultures(values);
  };

  const [results, setResults] = useState<Array<Item>>([]);

  const prepareParams = () => {
    return {
      culture: cultures.join('|'),
      century: centuries.join('|'),
      classification: classifications.join('|'),
    };
  };

  const onFormSubmit = (event) => {
    event.preventDefault();
    api
      .get('/object', {
        params: prepareParams(),
      })
      .then((res) =>
        setResults(
          res.data.records.map((el) => ({
            ...el,
            name: el.title,
            imageURL: toImageURL(
              el.baseimageurl ||
                el.primaryimageurl ||
                'https://upload.wikimedia.org/wikipedia/en/2/29/Harvard_shield_wreath.svg',
              300,
              300,
            ),
          })),
        ),
      )
      .catch((error) => setError(error));
  };

  useEffect(() => {
    Promise.all([
      api
        .get('/classification?size=58')
        .then((res) => setClassificationList(res.data.records))
        .catch((error) => setError(error)),
      api
        .get('/century?size=47')
        .then((res) => setCenturyList(res.data.records))
        .catch((error) => setError(error)),
      api
        .get('/culture?size=255')
        .then((res) => setCultureList(res.data.records))
        .catch((error) => setError(error)),
    ]).then(() => setLoading(false));
  }, []);

  if (loading) {
    return <Loader data-testid="loader" />;
  }

  if (error) {
    return <h2>Error happened: {error.message}</h2>;
  }

  return (
    <section className={styles.searchPage}>
      <form onSubmit={onFormSubmit} className={styles.form}>
        <Autocomplete
          multiple
          id="cultures-input"
          options={cultureList.map((el) => el.name)}
          defaultValue={[]}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" label="Select cultures" placeholder="Cultures" />
          )}
          onChange={onCulturesChange}
        />
        <Autocomplete
          multiple
          id="centuries-input"
          options={centuryList.map((el) => el.name)}
          defaultValue={[]}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" label="Select centuries" placeholder="Centuries" />
          )}
          onChange={onCenturiesChange}
        />
        <Autocomplete
          multiple
          id="classifications-input"
          options={classificationList.map((el) => el.name)}
          defaultValue={[]}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" label="Select classifications" placeholder="Classifications" />
          )}
          onChange={onClassificationsChange}
        />
        <Button
          id="submit"
          data-testid="submit"
          variant="contained"
          color="primary"
          type="submit"
          className={styles.submit}
        >
          Search!
        </Button>
      </form>

      {results.length > 0 && (
        <div data-testid="search_results">
          <ObjectList data={results} linkBase={'item'} />
        </div>
      )}
    </section>
  );
};

export default Search;

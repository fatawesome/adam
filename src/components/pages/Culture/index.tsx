import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { StateType } from '@main/__data__/reducers';
import { getCulture } from '@main/__data__/actions/culture';
import { Loader } from '@main/components/common/Loader';
import ObjectList from '@main/components/containers/ObjectList';
import styles from './styles.module.css';

const Culture = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const cultureData = useSelector((state: StateType) => {
    return state.culture;
  });

  useEffect(() => {
    dispatch(getCulture(id));
  }, []);

  if (cultureData.error) {
    return <h2 data-testid="error">Error happened: {cultureData.error}</h2>;
  }

  if (!cultureData.data || cultureData.loading) {
    return <Loader />;
  }

  return (
    <section>
      <h2 className={styles.title} data-testid="title">
        Discover artifacts of {cultureData.data.name} culture.
      </h2>
      <ObjectList data={cultureData.data.objects} linkBase={`item`} />
    </section>
  );
};

export default Culture;

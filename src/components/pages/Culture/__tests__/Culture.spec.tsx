import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { render, screen } from '@testing-library/react';
import { api } from '../../../../__data__/actions/common';
import { BrowserRouter } from 'react-router-dom';

import Culture from '../index';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import reducer from '../../../../__data__/reducers';
import * as Thunk from 'redux-thunk';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
  useParams: () => ({
    id: 1,
  }),
  useRouteMatch: () => ({ url: '/' }),
}));

describe('src/components/pages/Culture', () => {
  let mock = null;
  beforeEach(() => {
    mock = new MockAdapter(api);
    mock.onGet(/\/culture\/\d+/).reply(200, require('../../../../../stubs/api/culture.json'));
    mock.onGet('/object').reply(200, require('../../../../../stubs/api/object.json'));
  });

  describe('while loading', () => {
    it('renders loader', () => {
      mock = new MockAdapter(api, { delayResponse: 500 });
      render(
        <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
          <BrowserRouter>
            <Culture />
          </BrowserRouter>
        </Provider>,
      );
      expect(screen.findByTestId('loader')).toBeDefined();
    });
  });

  describe('when data is loaded', () => {
    it('renders culture page', async () => {
      const { container } = render(
        <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
          <BrowserRouter>
            <Culture />
          </BrowserRouter>
        </Provider>,
      );
      expect(await screen.findByTestId('title')).toBeDefined();
      expect(container.firstChild).toMatchSnapshot();
    });
  });

  describe('when api request throws error', () => {
    it('renders error', async () => {
      mock.onGet(/\/culture\/\d+/).reply(404, {});
      render(
        <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
          <BrowserRouter>
            <Culture />
          </BrowserRouter>
        </Provider>,
      );
      expect(await screen.findByTestId('error')).toBeDefined();
    });
  });
});

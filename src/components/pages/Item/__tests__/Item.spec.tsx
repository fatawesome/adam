import React from 'react';
import MockAdapter from 'axios-mock-adapter';
import { render, screen } from '@testing-library/react';
import { api } from '../../../../__data__/actions/common';
import { BrowserRouter } from 'react-router-dom';

import ItemPage from '../index';

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
  useParams: () => ({
    id: 1,
  }),
  useRouteMatch: () => ({ url: '/' }),
}));

describe('src/components/pages/Item', () => {
  let mock = null;
  beforeEach(() => {
    mock = new MockAdapter(api);
    mock.onGet().reply(200, require('../../../../../stubs/api/singleObject.json'));
  });

  describe('while loading', () => {
    it('renders loader', () => {
      mock = new MockAdapter(api, { delayResponse: 500 });
      render(
        <BrowserRouter>
          <ItemPage />
        </BrowserRouter>,
      );
      expect(screen.findByTestId('loader')).toBeDefined();
    });
  });

  describe('when data is loaded', () => {
    it('renders item page', async () => {
      const { container } = render(
        <BrowserRouter>
          <ItemPage />
        </BrowserRouter>,
      );
      expect(await screen.findByTestId('title')).toBeDefined();
      expect(container.firstChild).toMatchSnapshot();
    });
  });
});

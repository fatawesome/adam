import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import styles from './styles.module.css';
import { api } from '@main/__data__/actions/common';

import { Loader } from '@main/components/common/Loader';
import { Item } from '@main/__data__/model/item';
import { getImage, toImageURL } from '@main/utils';

const ItemPage = () => {
  const { id } = useParams();
  const [item, setItem] = useState<Partial<Item>>({ loading: true });
  const [error, setError] = useState(null);

  const getItem = async () => {
    try {
      const res = await api.get(`/object/${id}`);
      setItem(res.data);
    } catch (e) {
      setError(e);
    }
  };

  useEffect(() => {
    getItem();
  }, []);

  if (error) {
    return <h2>Error happened</h2>;
  }

  if (item.loading) {
    return <Loader />;
  }

  return (
    <section className={styles.itemPage}>
      <h2 data-testid="title">{item.title}</h2>
      <div className={styles.itemContent}>
        <ul className={styles.featureList}>
          <li>
            <span className={styles.featureTitle}>Culture: </span>
            {item.culture}
            {item.period && `, ${item.period}`}
          </li>
          <li>
            <span className={styles.featureTitle}>Made of: </span>
            {item.medium}
          </li>
          <li>
            <span className={styles.featureTitle}>Verification level: </span>
            {item.verificationleveldescription}
          </li>
        </ul>

        <div className={styles.imageContainer}>
          <img src={toImageURL(getImage(item))} alt="Item image" />
        </div>
      </div>
    </section>
  );
};

export default ItemPage;

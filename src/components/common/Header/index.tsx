import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import styles from './styles.module.css';

const Header = () => (
  <header className={styles.header}>
    <Link to="/" className={styles.logoLink}>
      <span className={styles.logo}>Harvard Art Museums</span>
    </Link>
    <nav className={styles.navigation}>
      <ul className={styles.navigationList}>
        <li>
          <NavLink to="/search" className={styles.link} activeClassName={styles.linkActive}>
            Search
          </NavLink>
        </li>
        <li>
          <NavLink to="/cultures" className={styles.link} activeClassName={styles.linkActive}>
            Cultures
          </NavLink>
        </li>
      </ul>
    </nav>
  </header>
);

export default Header;

import React from 'react';
import styles from './styles.module.css';

export const Loader = () => (
  <div className={styles.wrapper} data-testid="loader">
    <div className={styles.loader} />
  </div>
);

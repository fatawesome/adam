import React from 'react';
import renderer from 'react-test-renderer';

import ObjectList from '../index';
import { BrowserRouter } from 'react-router-dom';

describe('components/containers/ObjectList', () => {
  it('renders', () => {
    const data = [
      {
        id: 1,
        imageURL: 'imageURL',
        name: 'name',
        title: 'title',
      },
    ];
    const tree = renderer
      .create(
        <BrowserRouter>
          <ObjectList data={data} linkBase={''} />
        </BrowserRouter>,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

import React from 'react';
import { Link } from 'react-router-dom';

import styles from './styles.module.css';
import ObjectCard from '@main/components/containers/ObjectCard';

type ObjectListProps<ObjectType> = {
  data: Array<ObjectType>;
  linkBase: string;
};

const ObjectList = <
  ObjectType extends {
    id: number;
    imageURL?: string;
    name?: string;
  }
>({
  data,
  linkBase,
}: ObjectListProps<ObjectType>) => (
  <div className={styles.objectGrid} data-testid="object_list">
    {data.map((elem) => (
      <Link to={`/${linkBase}/${elem.id}/`} key={elem.id}>
        <ObjectCard object={elem} />
      </Link>
    ))}
  </div>
);

export default ObjectList;

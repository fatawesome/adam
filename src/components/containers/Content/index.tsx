import React from 'react';
import styles from './styles.module.css';

// eslint-disable-next-line react/prop-types
const Content = ({ children }) => <div className={styles.content}>{children}</div>;

export default Content;

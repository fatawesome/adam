import React from 'react';
import { render } from '@testing-library/react';
import Container from '../index';
import { BrowserRouter } from 'react-router-dom';

describe('src/components/common/Header', () => {
  it('renders', () => {
    const { container } = render(
      <BrowserRouter>
        <Container>test</Container>
      </BrowserRouter>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});

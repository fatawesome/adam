import React from 'react';
import renderer from 'react-test-renderer';

import ObjectCard from '../index';

describe('components/containers/ObjectCard', () => {
  it('renders', () => {
    const obj = {
      id: 1,
      imageURL: 'imageURL',
      name: 'name',
      title: 'title',
    };
    const tree = renderer.create(<ObjectCard object={obj} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  describe('when there is no name', () => {
    it('renders title', () => {
      const obj = {
        id: 1,
        imageURL: 'imageURL',
        title: 'title',
      };
      const tree = renderer.create(<ObjectCard object={obj} />).toJSON();
      expect(tree).toMatchSnapshot();
    });
  });
});

import React from 'react';
import styles from './styles.module.css';

interface ObjectCardProps<ObjectType> {
  object: ObjectType;
}

const ObjectCard = <
  ObjectType extends {
    id: number;
    imageURL?: string;
    name?: string;
    title?: string;
  }
>({
  object,
}: ObjectCardProps<ObjectType>) => (
  <section className={styles.objectCard}>
    <div className={styles.imageContainer}>
      <img src={object.imageURL} alt={object.name} className={styles.image} />
    </div>
    <h3 className={styles.name}>{object.name || object.title}</h3>
  </section>
);

export default ObjectCard;
